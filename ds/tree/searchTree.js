/*
 * Implement Search Tree and indicate its applications
 * n is the number objects in the tree
 * Best case scenario O(logn) / O(h)
 * Worst case scenario n - 1
 *
 */

class SearchTree {
  constructor() {}
  search() {}
  min() {}
  max() {}
  predecessor() {} // next smallest key
  successor() {}
  outputSorted() {} // in - order traversal
  insert() {}
  delete() {}
  select() {}
  rank() {}
}
