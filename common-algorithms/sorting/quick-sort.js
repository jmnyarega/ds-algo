/*
 *
 * Quick algorithm uses in-place mechanism -> Reduces space complexity. No additional space grows with 
 * the input... WIN Uses swaps instead.
 */

function partition(A, startIndex, endIndex) {
  let pivot = A[endIndex];
  let leftIndex = startIndex;
  let rightIndex = endIndex - 1;
  while (leftIndex <= rightIndex) {
    while (leftIndex <= endIndex && A[leftIndex] < pivot) { leftIndex++; }
    while (rightIndex >= startIndex && A[rightIndex] > pivot) { rightIndex--; }

    if (leftIndex < rightIndex) {
      // swap
      const temp = A[rightIndex];
      A[rightIndex] = A[leftIndex];
      A[leftIndex] = temp;
    }
    else {
      // This means left and right have crossed, we know that all the items smaller than - 
      // the pivot are before left and all items larger than the pivot are after right
      // This will be executed 4 times because, the array will be partitioned 4 times
      const temp = A[endIndex];
      A[endIndex] = A[leftIndex];
      A[leftIndex] = temp;
    }
  }
  return leftIndex; // this will be the new pivot...
}

function quickSort(A, startIndex, endIndex) {
  if (startIndex >= endIndex) return;
  const pivotIndex = partition(A, startIndex, endIndex);
  quickSort(A, startIndex, pivotIndex - 1);
  quickSort(A, pivotIndex + 1, endIndex);
  return A;
}

const array = [56, 26, 93, 17, 77, 31, 44, 55, 20];
const sorted = quickSort(array, 0, array.length - 1);
console.log(sorted);
