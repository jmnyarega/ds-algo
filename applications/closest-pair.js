/*
 * The closest pair problem concers points (x, y) E R^2 in the plane.
 * To measure the distance between two points p1 = (x1, y1) and p2 = (x2, y2), - 
 * we use Euclidean (straight - line) distance
 * d(p1,p2) = square-root of ((x1 - x2)^2 + (y1 - y2)^2)
 *
 * @TODO study about this problem in depth & implement the solution in code.
 *
 * 1. Implement it using brute-force 
 * 2. Use deivide and conquire
 *
 */
